package ui;

import DataAccess.DataBaseException;
import DataAccess.MyCourseRepository;
import DataAccess.StudenRepository;
import Exceptions.InvalidValidValueException;
import domain.Course;
import domain.CourseType;
import domain.Student;

import java.sql.Date;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class Cli {
    Scanner scan;
    MyCourseRepository repo;
    StudenRepository stdRepo;
    public Cli(MyCourseRepository myCourseRepository, StudenRepository studenRepository){
        this.scan = new Scanner(System.in);
        this.repo = myCourseRepository;
        this.stdRepo = studenRepository;
        start();
    }

    public void start(){
        String input = "-";
        while (!input.equals("x")){
            showMenu();
            input = scan.nextLine();
            switch (input){
                case "1":
                    System.out.println("Kurseingabe: ");
                    addCourse();
                    break;
                case "2":
                    System.out.println("Alle Kurse Anzeigen");
                    showAllCourses();
                    break;
                case "3":
                    System.out.println("Kurs nach ID");
                    showCourseDetails();
                    break;
                case "4":
                    System.out.println("Update");
                    updateCourse();
                    break;
                case "5":
                    System.out.println("Loeschen eines Kurses");
                    deleteCourse();
                    break;
                case "6":
                    System.out.println("Kurssuche");
                    kurssuche();
                    break;
                case "7":
                    System.out.println("Running Courses");
                    runningCourses();
                    break;
                case "8":
                    System.out.println("New Student: ");
                    newStudent();
                    break;
                case"9":
                    System.out.println("Alle Students anzeigen");
                    showAllStudents();
                    break;
                case"10":
                    System.out.println("Student Anzeigen");
                    showStudenDetails();
                    break;
                case"11":
                    System.out.println("Student Aendern");
                    updateStudent();
                    break;
                case"12":
                    System.out.println("Student uber Vor oder Nachname suchen");
                    studentFindByName();
                    break;
                case"13":
                    System.out.println("Student nach Jahr");
                    findStundentByYear();
                    break;
                case"14":
                    System.out.println("Student Nach Vorname suchen");
                    findStudentByFirstName();
                    break;
                case "x":
                    System.out.println("Auf wiedersehen");
                    break;
                default:
                    inputError();
                    break;
            }
        }
        scan.close();
    }

    private void findStudentByFirstName(){
        System.out.println("Bitte geben Sie einen Vorname an: ");
        String searchString = scan.nextLine();

        try{
            List<Student> students = stdRepo.getStudentsWithFirstName(searchString);
            for(Student student : students){
                System.out.println(student);
            }
        }catch (DataBaseException dataBaseException){
            System.out.println("Fehler bei Studentsuche : " + dataBaseException.getMessage());
        }catch (Exception e){
            System.out.println("Unbekannter Fehler bei der Studentsuche " + e.getMessage());
        }

    }
    private void findStundentByYear(){
        System.out.println("Bitte geben Sie ein Jahr an: ");
        String searchString = scan.nextLine();

        try{
            List<Student> students = stdRepo.getStudentsFromYear(searchString);
            for(Student student : students){
                System.out.println(student);
            }
        }catch (DataBaseException dataBaseException){
            System.out.println("Fehler bei Studentsuche nach Jahr : " + dataBaseException.getMessage());
        }catch (Exception e){
            System.out.println("Unbekannter Fehler bei der Studentsuche " + e.getMessage());
        }
    }
    private void studentFindByName(){
        System.out.println("Bitte geben Sie einen Name an: ");
        String searchString = scan.nextLine();

        try{
            List<Student> students = stdRepo.getStudentsByLastOrFirstName(searchString);
            for(Student student : students){
                System.out.println(student);
            }
        }catch (DataBaseException dataBaseException){
            System.out.println("Fehler bei Studentsuche : " + dataBaseException.getMessage());
        }catch (Exception e){
            System.out.println("Unbekannter Fehler bei der Studentsuche " + e.getMessage());
        }
    }
    private void updateStudent(){
        String firstname, lastname, gebdate;
        Long id = null;

        System.out.println("Welchen Kurs (id) moechten Sie updaten? ");
        id = Long.parseLong(scan.nextLine());

        try {
            Optional<Student> studentOptional = stdRepo.getByID(id);
            if(studentOptional.isEmpty()){
                System.out.println("Kurs mit der ID nicht in der Datenbank!");
            }else {
                Student student = studentOptional.get();
                System.out.println("Aenderungen fuer den folgenden Kurs");
                System.out.println(student);
                System.out.println("Bitte neue kursdaten angeben: (Falls keine Aenderungen erwünscht sind ENTER) ");
                System.out.println("Vorname");
                firstname = scan.nextLine();
                System.out.println("Nachname: ");
                lastname = scan.nextLine();
                System.out.println("Geburtsdatum (YYYY-MM-DD): ");
               gebdate = scan.nextLine();

                Optional<Student> optionalStudent = stdRepo.update(
                        new Student(
                                student.getId(),
                                firstname.equals("") ? student.getVorname() : firstname,
                                lastname.equals("") ? student.getNachname() : lastname,
                                gebdate.equals("") ? student.getGebDate() : Date.valueOf(gebdate)
                        )
                );

                optionalStudent.ifPresentOrElse(
                        (c) -> System.out.println("Student wurde aktualisiert " + c),
                        () -> System.out.println("Student konnte nicht aktualisiert werden ")
                );
            }

        }catch (DataBaseException dataBaseException){
            System.out.println("Datenbankfehler beim Update Student " + dataBaseException.getMessage());
        }
        catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Illegealargument Exception beim Einfuegen " + illegalArgumentException.getMessage());
        }
        catch (Exception e){
            System.out.println("Unbekannter Fehler beim Update Student: " + e.getMessage() );
        }

    }
    private void showStudenDetails(){
        System.out.println("Für Welchen Studen möchten Sie die Details anzeigen? ");
        Long id = Long.parseLong(scan.nextLine());
        try {
            Optional<Student> student = stdRepo.getByID(id);
            if(student.isPresent()){
                System.out.println(student.get());
            }
            else{
                System.out.println("Student mit der ID " + id + " nicht gefunden");
            }
        }catch (DataBaseException dataBaseException){
            System.out.println("Datenbankfehler bei Studentdetailanzeige" + dataBaseException.getMessage());
        }catch (Exception e){
            System.out.println("Fehler bei Studentdetailanzeige " +e.getMessage());
        }
    }

    private void showAllStudents(){
        List<Student> list = null;
        list = stdRepo.getAll();
        try{
            if(list.size() > 0){
                for(Student student: list){
                    System.out.println(student.toString());
                }
            }
            else {
                System.out.println("Studentenliste Leer");
            }
        }
        catch (DataBaseException dataBaseException){
            System.out.println("Datenbankfehler bei Anzeige aller Studenten " + dataBaseException.getMessage());
        }
        catch (Exception exception){
            System.out.println("Unbekannter Fehler " + exception);
        }
    }
    private void newStudent(){
        String firstname, lastname, gebDate;

        try {
            System.out.println("Neunen Studen Anlegen: ");
            System.out.println("Vorname: ");
            firstname = scan.nextLine();
            System.out.println("Nachname");
            lastname = scan.nextLine();
            System.out.println("Geburtstag: (YYYY-MM-DD):");
            gebDate = scan.nextLine();

            Optional<Student> optionalStudent = stdRepo.insert(
                    new Student(
                            firstname, lastname, Date.valueOf(gebDate)
                    ));
            optionalStudent.ifPresentOrElse(
                    (c) -> System.out.println("Student wurde angelegt " + c),
                    () -> System.out.println("Student konnte nicht angelegt werden ")
            );

        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Illegal Argument bei Student Anlegen " + illegalArgumentException.getMessage());
        }

    }
    private void runningCourses(){
        try {
            List<Course> courseList =  repo.findAllRunningCourses();
            if(courseList.isEmpty()){
                System.out.println("Es Laufen momentan keine Kurse: ");
            }else{
                System.out.println("Die Laufenden Kurse werden ausgegeben: ");
            }
            for(Course course : courseList){
                System.out.println(course);
            }
        }catch (DataBaseException dataBaseException){
            System.out.println("Fehler beim Suchen nach allen laufenden Kursen " + dataBaseException.getMessage());
        }catch (Exception e){
            System.out.println("Unbekanter Fehler beim ausgeben aller laufenden Kurse: " + e.getMessage());
        }
    }

    private void kurssuche(){
        System.out.println("Bitte geben Sie einen Kursbegriff an: ");
        String searchString = scan.nextLine();

        try{
          List<Course> courseList= repo.findAllCourseByNameOrDescription(searchString);
          for(Course course : courseList){
              System.out.println(course);
          }
        }catch (DataBaseException dataBaseException){
            System.out.println("Fehler bei Kurssuche : " + dataBaseException.getMessage());
        }catch (Exception e){
            System.out.println("Unbekannter Fehler bei der Kurssuche " + e.getMessage());
        }
    }

    private void deleteCourse(){
        System.out.println("Welchen Kurs moechten Sie loeschen? (bitte ID eingeben): ");
        Long courseIdToDelete = Long.parseLong(scan.nextLine());

        try {
            repo.deleteById(courseIdToDelete);

        }catch (DataBaseException dataBaseException){
            System.out.println("Datenbankfehler beim loeschen " + dataBaseException.getMessage());
        }
        catch (Exception e){
            System.out.println("Unbekannter Fehler beim Loeschen" + e.getMessage());
        }
    }

    private void updateCourse(){
        String name, description, hours, dateFrom, dateTo, courseType;
        Long id = null;

        System.out.println("Welchen Kurs (id) moechten Sie updaten? ");
        id = Long.parseLong(scan.nextLine());

        try {
            Optional<Course> courseOptional = repo.getByID(id);
            if(courseOptional.isEmpty()){
                System.out.println("Kurs mit der ID nicht in der Datenbank!");
            }else {
                Course course = courseOptional.get();
                System.out.println("Aenderungen fuer den folgenden Kurs");
                System.out.println(course);
                System.out.println("Bitte neue kursdaten angeben: (Falls keine Aenderungen erwünscht sind ENTER) ");
                System.out.println("Name");
                name = scan.nextLine();
                System.out.println("Beschreibung: ");
                description = scan.nextLine();
                System.out.println("Stundenanzahl");
                hours = scan.nextLine();
                System.out.println("Startdatum (YYYY-MM-DD): ");
                dateFrom = scan.nextLine();
                System.out.println("Enddatum (YYYY-MM-DD): ");
                dateTo = scan.nextLine();
                System.out.println("KursTypen: OE, BF, ZA, SW, RU, FF ");
                courseType = scan.nextLine();

                Optional<Course> optionalCourseUpdatet = repo.update(
                        new Course(course.getId(),
                                name.equals("") ? course.getName() : name,
                                description.equals("") ? course.getDescription() : description,
                                hours.equals("")? course.getHours() : Integer.parseInt(hours),
                                dateFrom.equals("") ? course.getBeginDate() : Date.valueOf(dateFrom),
                                dateTo.equals("") ? course.getEndDate() : Date.valueOf(dateTo),
                                courseType.equals("") ? course.getCourseType() : CourseType.valueOf(courseType)
                                ));

                optionalCourseUpdatet.ifPresentOrElse(
                        (c) -> System.out.println("Kurs wurde aktualisiert " + c),
                        () -> System.out.println("Kurs konnte nicht aktualisiert werden ")
                );
            }

        }catch (DataBaseException dataBaseException){
            System.out.println("Datenbankfehler beim Update " + dataBaseException.getMessage());
        }
        catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Illegealargument Exception beim Einfuegen " + illegalArgumentException.getMessage());
        }
        catch (Exception e){
            System.out.println("Unbekannter Fehler beim Update: " + e.getMessage() );
        }

    }

    private void addCourse() {
        String name, description;
        int hours;
        Date dateFrom, dateTo;
        CourseType courseType;
        Long id = null;

        try {
            System.out.println("Bitte alle kursdaten angeben: ");
            System.out.println("Name");
            name = scan.nextLine();
            if(name.equals("")){throw new IllegalArgumentException("Eingabe darf nicht leer sein");}
            System.out.println("Beschreibung: ");
            description = scan.nextLine();
            if(description.equals("")){throw new IllegalArgumentException("Eingabe darf nicht leer sein");}

            System.out.println("Stundenanzahl");
            hours = Integer.parseInt(scan.nextLine());

            System.out.println("Startdatum (YYYY-MM-DD): ");
            dateFrom = Date.valueOf(scan.nextLine());
            System.out.println("Enddatum (YYYY-MM-DD): ");
            dateTo = Date.valueOf(scan.nextLine());
            System.out.println("KursTypen: OE, BF, ZA, SW, RU, FF ");
            courseType = CourseType.valueOf(scan.nextLine());

            Optional<Course> optionalCourse = repo.insert(
                    new Course(name, description, hours, dateFrom, dateTo, courseType)
            );
            if (optionalCourse.isPresent()){
                System.out.println("Kurs angelegt "+ optionalCourse.get());
            }else{
                System.out.println("Kurs konnte nicht angelegt werden. ");
            }


        }catch (IllegalArgumentException illegalArgumentException){
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        }catch (InvalidValidValueException invalidValidValueException){
            System.out.println("Kursdaten nicht Korrekt " + invalidValidValueException.getMessage() );
        }catch (DataBaseException dataBaseException){
            System.out.println("Datenbankfehler beim einfuegen " + dataBaseException.getMessage());
        }catch (Exception exception){
            System.out.println("Sonstiger Fehler " + exception.getMessage());
        }
    }

    private void showCourseDetails() {
        System.out.println("Für Welchen Kurs möchten Sie die Details anzeigen? ");
        Long id = Long.parseLong(scan.nextLine());
        try {
            Optional<Course> course = repo.getByID(id);
            if(course.isPresent()){
                System.out.println(course.get());
            }
            else{
                System.out.println("Kurs mit der ID " + id + " nicht gefunden");
            }
        }catch (DataBaseException dataBaseException){
            System.out.println("Datenbankfehler bei Kursdetailanzeige" + dataBaseException.getMessage());
        }catch (Exception e){
            System.out.println("Fehler bei Kursdetailanzeige " +e.getMessage());
        }

    }

    private void showAllCourses() {
        List<Course> list = null;
        list = repo.getAll();
        try{
            if(list.size() > 0){
            for(Course course : list){
                System.out.println(course.toString());
                }
            }
            else {
            System.out.println("Kursliste Leer");
         }
        }
        catch (DataBaseException dataBaseException){
            System.out.println("Datenbankfehler bei Anzeige aller Kurse " + dataBaseException.getMessage());
        }
        catch (Exception exception){
            System.out.println("Unbekannter Fehler " + exception);
        }
    }

    public void showMenu(){
        System.out.println("------ KURSMANAGEMENT-----");
        System.out.println("1) Kurs eingeben \t 2) Alle Kurse Anzeigen \t 3) Kursdeatail Anzeigen");
        System.out.println("4) Kursdetails aendern  \t 5) Kurs loeschen \t 6) Kurssuche \t 7) Running Courses");
        System.out.println("8) Students anlegen \t 9) Alle Students anzeigen  \t 10)Student Anzeigen ");
        System.out.println("11) Student Aendern \t 12) Student uber Vor oder Nachname suchen \t 13) Student nach Jahr ");
        System.out.println("14) Student Nach Vorname suchen");
        System.out.println("x) Ende");
    }

    private void inputError(){
        System.out.println("Eingabe wurde falsch getaetigt.");
    }
}
