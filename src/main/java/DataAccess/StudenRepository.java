package DataAccess;

import domain.Course;
import domain.Student;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface StudenRepository extends BaseRepository<Student, Long>{
   List<Student> getStudentsByLastOrFirstName(String name);
   List<Student> getStudentsFromYear(String year);
   List<Student> getStudentsWithFirstName(String firstName);

}
