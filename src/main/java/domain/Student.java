package domain;

import Exceptions.InvalidValidValueException;

import java.sql.Date;

public class Student extends BaseEntity {
    private String vorname, nachname;
    private Date gebDate;
    private Long id;

    public Student(Long id, String vorname, String nachname, Date gebDate) {
        super(id);
        this.setVorname(vorname);
        this.setNachname(nachname);
        this.setGebDate(gebDate);
    }
    public Student( String vorname, String nachname, Date gebDate) {
        super(null);
        this.setVorname(vorname);
        this.setNachname(nachname);
        this.setGebDate(gebDate);
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) throws InvalidValidValueException{
        if(vorname != null && vorname.length() >= 1){
            this.vorname = vorname;
        }else {
            throw new InvalidValidValueException("Vorname darf nicht Leer sein! ");
        }
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
       if(nachname != null && nachname.length() >= 1){
           this.nachname = nachname;
       }else {
           throw new InvalidValidValueException("Nachname darf nicht Leer sein! ");
       }

    }

    public Date getGebDate() {
        return gebDate;
    }

    public void setGebDate(Date gebDate) {
       if(gebDate != null){
           this.gebDate = gebDate;
       }else{
           throw new InvalidValidValueException("Geburtstag darf nicht Leer sein");
       }
    }

    @Override
    public String toString() {
        return "Student{" +
                "vorname='" + vorname + '\'' +
                ", nachname='" + nachname + '\'' +
                ", gebDate=" + gebDate +
                ", id=" + getId() +
                '}';
    }
}
