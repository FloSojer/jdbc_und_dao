#Dokumentation zu JDBC und DAO #
##Was ist JDBC ##
J - Java </br>
DB - Database </br>
C - Connection </br>

## Was ist DAO ## 
D - Data </br>
A - Access </br>
O - Object </br>

In diesem Projekt geht es darum, die Geschäftslogik nicht direkt 
über die Datenbank zu entwickeln sondern verarbeitungsporzesse im Hintergrund zu verstecken.
Dabei wird die Erweiterbarkeit des Programms und Softwarequalität erhöht. 

## Einrichtung der Datenbank ## 
Ich habe meine Datenbank dazu auf einem Docker Container eingerichtet, da die 
Verwertbarkeit und der Zugriff auf MacOs mir leichter gefallen ist. 

## Datenbank verbindung einrichten ## 
Dabei erstellen wir ein eigenes Package um die Struktur etwas zu verbessern.
Danach eine eigene Klasse mit privaten Konstruktor. Der Konstruktor ist, privat, dass nicht immer ein neues Objekt daraus erstellt wird. 
Als nächstes, richten wir eine Funktion 'Conncection' ein. Dabei wird als erstes Überprüft, ob eine Verbindung bereits besteht und nur wenn nicht, wird über Connection eine neue
Verbindung aufgebaut und diese Zurückgegeben. </br>

##Komandozeilemenü##
Dabei wird eine Klasse als eigenes Kommandozeilenmenü implementiert. 
Die Kommandozeile verläuft in einer While-Schleife und über Switch-Case Abfragen koennen
diverse Kommandos und deren Funktionen ausgefuehrt werden. 

## Implementierung Domänenklasse ## 
Dabei wurde als erstes die ID als Basis Entität in einer Abstrakten Klasse 
definiert. Dabei darf, falls eine ID erneuert wird, diese nicht null oder <0 sein. 
Danach wurde eine Klasse, zum setzen der Werte innerhalb der Businesslogic implementiert. Hier werden die eingegebenen Werte 
auf korrekte Eingabe (!null oder >0) überprüft. Dabei wurden die Kurstypen als Enum festgelgt. 

## Implementierung DAO Interface ## 
Das Interface besitzt nur Methoden wie zb. Safe. Dabei verwendet die CLI Klasse das
DAO Interface um den SQL Zugriff zu Realisieren, obwohl keine Direkter Zugriff auf die Datenbank möglich gemacht wird.
Das SQL-DAO stellt die funktionen lediglich fertig und erledigt somit auch den Datenbankzugriff. 
Dabei kreirt man ein Interface des namens BaseRepository welche TypInformationen zurückgeben wollen und deren Funktionen liefern einen Wert bzw Werte des Typ Optional zurück.
Denn durch das Optional bleiben die möglichkeiten des Datentyps offen. 
</br>
Danach wird das CourseRepository eingefügt. Dies ist das eigentliche DAO Interface. 
Eigentlich könnte man alle Methoden auch über das KursRepo Interface generieren, jedoch sind sie über das BaseRepository schon fix getypt
und können für jedes weitere Repository so erweitert werden. 

## GetAll Implementierung Teil 1 ## 
Hier werden die CRUD Funktionalitäten implementiert. 
Dadurch im CourseRepository Interface die Rückgaben getypt wurden, erkennt das System, dass statt dem Optional Courses verwendet werden müsen.
Dabei wird schon vom Objektrelationalen Mapping gesprochen. Somit muss auch der Programmierer nichts mehr über die Datenbank wissen.
Danach wird in diesem Fall das GetAll Statement mit Hilfe von Connection implementiert.
Das resultset.next liefert lediglich wieder ein true oder false wert, ob ein neuer Wert verfügbar ist, zurück. 
Die Liste aller Ergbnisse wird in einer Liste gespeichert und diese wird, sofern kein Fehler auftritt zurückgegeben. 
</br>
In der CLI wird über dem Header das CourseRepository mitgegeben und damit auch alle Funktionen, ohne das SQL Statement anzugreifen.
Die Liste kann dann über die ausimplementierte Funktion "getAll" gezogen werden.
Ist die Liste nicht leer, werden alle Daten mit Hilfe der toString Methode ausgegeben. 
Danach muss das CLI nur in der Main Klasse ausgeführt werden.

## GetAll Implementierung Teil 2 ## 
Dabei wurde das Prgramm noch einmal durchgesprochen und getestet. 
Ebenfalls wurde der Fehler innerhalb der While Schleife behoben.

## Get By ID ##
Dazu wurde ein neues Package angelegt welches die Assert Klasse beinhaltet. Die Assertklasse erledigt die Nullchecks. 
Danach wurde in der MySQLRepository Klsse, das getByID ausimplementiert. Dabei wurde als erstes herausgefiltert ob der WERt der ID 0 ist, bzw ob es 
verfügbare Daten für diese ID gibt. Wenn es keine gibt, wird ein Leeres Optionales Objekt zurückgegeben. 
Wenn es Werte für diese ID gibt, wird ein SQL Statement mit Hilfe eines Prepared Statements über die Connection festgelegt.
Das Resultset beinhaltet die zurückgegebenen Werte eines ausgeführten Befehls.
Über die funtktionen des Resultset, werden die Werte dem Objekt hinzugefügt welches dann zurückgegeben wird.
Danach wurde in der CLI eine Neue Funktion erstellt, welche den Übergebenen Parameter Überprüft und danach an das Repo weitergibt.
Das Repo verarbeitet diese Funktion dann nur noch in dem SQLRepository. 

## Create Methode ##
Als erstes wird in der Create Funktion das SQL Statement vorbereitet. Dabei werden die Werte als Frageziechen gesetzt. 
Danach werden die vom erstellten Kurs angelegeten Werte in das Prepares Statement weitergegeben. 
Das einfügen der Werte erfolgt danach über das Execute Update, dabei wird die Zahl der betroffenen Reiheen
zurückgegeben. Wurde keine Zeile davon betroffen, wird ein leeres Objekt zurückgeben und ansonsten
holt man sich die Reihe über die erstellte ID. Dies funktioniert über das Resultset.GeneretedKeys. 
Dabei wird die neu generierte ID der Datenbank zurückgeliefert. 
Zuletzt erzeugt man in der CLI klasse eine neue funktion, welche ein neues Kursobjekt erzeugt als Optional und
dieses dann nach an das repo.insert übergibt. 

##Update Methode ##
Als erstes wird wieder in der MySQLCourseRepository die update Methode ausimplementiert.
Dabei wird als erstes geprüft, ob die übergebene Entität Null ist, ist diese Null, wird sofort eine Exeption geworfen.
Dann wird das Update-SQL-Statement in einem String zur weiterverarbeitung gespeichert. 
Danach wird das Statement wieder über die verbindung vorbereitet und mit dem werten des Übergebenen Objekts befüllt.
Wenn das Statement über "execute" ausgeführt wird, gibt es lediglich die Anzahl der betroffenen 
reihen zurück. Ist keine reihe betroffen, wird ein leeres Objekt zurückgegben und ansonsten das aktualisierte.
Im CLI wird dann ein neuer Punkt mit einer neuen Funktion angelegt. Dabei wird als erstes abgefragt, ob der Kurs vorhanden ist,
ist er es, wird der User aufgerufen seine Daten einzugeben. Die eingegebenen Daten werden dann an ein 
Optionales Objekt vom Typ kurs mit einem neuen Kursobjekt übergeben. Bei der erstellung des neuen kurses, wird in der Übergabe
mit hilfe eines ternery Operators überprüft ob einie eingabe getätigt wurde oder nicht. Ist keine Eingabe getätigt worden,
so wird der Wert des vorherigen Kurses übernommen, ansonsten der neue Wert eingefuegt.
Danach kann mit einer funktion von Optional ("ifPresentOrElse) überprüft werden ob der Kurs aktualiesiert wurde oder nicht.
Dabei wird über das (c) der neue Kurs mitgegeben, wenn nicht, spricht die zweite funktion an. 
Ist alles implementiert worden, kann die funktion getestet werden.

## Delete By ID ## 
Zuerst wird im MySQLCourseRepository die Funktion "deleteById" ausimplementiert.
Dabei legt man wieder ein SQL Statement als String an. Danach überprüft mit der Übergebenen Variable
ein Kurs zu finden ist, ist keiner zu finden, muss auch keiner gelöscht werden.
Wenn jedoch einer gelöscht werden muss, erledigt man dies wieder über ein PreparedStatement und die ausführung wird als Update durchgeführt.
Als letzteres wird die CLI ergänzt. Dafür kann wieder über das repo auf die CRUD funktionen zugegriffen werden und es wird lediglich die Delete funktion ausgeführt.

## find All Course By Name or Desoription ##
Dabei einfach nach der Prozedur, wie die Crud Methoden ausimplementiert wurden, auch die funktion welche das 
Kurs speziefische Repo zur vefügung stellt ausimplementieren. Dabei wird wieder eine ArrayList mit Kursen zurückgegeben.
Zuletzt dies nur noch in der CLI hinzufuegen. 

## find All running Courses ##
Dabei einfach nach der Prozedur, wie die Crud Methoden ausimplementiert wurden, auch die funktion welche das
Kurs speziefische Repo zur vefügung stellt ausimplementieren. Hier erfolgt das gleiche wie vorhin. 
Lediglich herrscht in diesem Teil, meiner Meinung nach ein Logischer Fehler: Es sollte unter anderem auch auf die Beginnzeit geachtet werden. 




