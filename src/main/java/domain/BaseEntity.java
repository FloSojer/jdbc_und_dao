package domain;

import Exceptions.InvalidValidValueException;

public abstract class BaseEntity {

    private Long id;

    public BaseEntity(Long id){
        setId(id);
    }
    public Long getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                '}';
    }

    public void setId(Long id) {
        if(id == null || id >= 0){
            this.id = id;
        }else{
            throw new InvalidValidValueException("Kurs ID muss >= 0 sein");
        }


        //this.id = id;
    }
}

