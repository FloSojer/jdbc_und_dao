package DataAccess;

import domain.Course;
import domain.CourseType;
import domain.Student;
import util.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySQLStudentRepository implements StudenRepository {
    private Connection con;
    private String url = "jdbc:mysql://0.0.0.0:3306/JDBC_und_DAO";
    private String user = "root";
    private String pwd = "DieBohne";

    public MySQLStudentRepository() throws SQLException, ClassNotFoundException {
        this.con = MySQLDatabaseConnection.getConnection(this.url, this.user, this.pwd);
    }


    @Override
    public Optional<Student> insert(Student entity ) {
        Assert.notNull(entity);
        // id = null + name + description + hours + begindate + enddate + CourseType
        String sql = "INSERT INTO JDBC_und_DAO.students VALUES (null, ?, ?, ?);";
        try {
        PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getVorname());
            preparedStatement.setString(2, entity.getNachname());
            preparedStatement.setDate(3, entity.getGebDate());

            int effectedRows = preparedStatement.executeUpdate();

            if(effectedRows == 0){
                return Optional.empty();
            }else{
                ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                if(generatedKeys.next()){
                    return this.getByID(generatedKeys.getLong(1));
                }else {
                    return Optional.empty();
                }
            }

        }catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }
    }

    @Override
    public Optional<Student> getByID(Long id) {
        Assert.notNull(id);
        if(countStudentsInDBWithId(id)==0){
            return Optional.empty();
        }
        else {
            String sql = "SELECT * FROM JDBC_und_DAO.students WHERE id = ?;";
            try {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                Student student = new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("gebdate")
                );
                return Optional.of(student);
            }
            catch (SQLException sqlException){
                throw new DataBaseException(sqlException.getMessage());
            }
        }

    }

    @Override
    public List<Student> getAll() {
        String sql = "SELECT * FROM JDBC_und_DAO.students; ";
        try{
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentArrayList = new ArrayList<>();
            while (resultSet.next()){
                studentArrayList.add( new Student(
                                resultSet.getLong("id"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getDate("gebdate")
                        )

                );

            }
            return studentArrayList;
        }catch (SQLException e) {
            throw new DataBaseException("Databaseerror Occured");
        }
    }

    @Override
    public Optional<Student> update(Student entity) {
        Assert.notNull(entity);
        String sql = "UPDATE JDBC_und_DAO.students SET firstname = ?, lastname = ?, gebdate = ? WHERE id = ?;";
        if(countStudentsInDBWithId(entity.getId()) == 0){
            return Optional.empty();
        }else {
            try{
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getVorname());
            preparedStatement.setString(2, entity.getNachname());
            preparedStatement.setDate(3, entity.getGebDate());
            int affectedRows = preparedStatement.executeUpdate();
                if(affectedRows == 0){
                    return Optional.empty();
                }else {
                    return this.getByID(entity.getId());
                }

        }catch (SQLException sqlException){
                throw new DataBaseException(sqlException.getMessage());
            }

        }
    }


    @Override
    public void deleteById(Long id) {
        Assert.notNull(id);
        try {
            String sql = "DELETE FROM JDBC_und_DAO.students WHERE id = ?";
            if (countStudentsInDBWithId(id) == 1) {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong( 1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> getStudentsByLastOrFirstName(String name) {
        try {
            String sql = "SELECT * FROM JDBC_und_DAO.students WHERE LOWER(`firstname`) LIKE LOWER(?) OR LOWER(`lastname`) LIKE LOWER(?)  ";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%"+name+ "%");
            preparedStatement.setString(2, "%"+name + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentArrayList = new ArrayList<>();
            while (resultSet.next()){
                studentArrayList.add(
                        new Student(
                                resultSet.getLong("id"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getDate("gebdate")
                        )
                );
            }
            return studentArrayList;

        }catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> getStudentsFromYear(String year) {
        try {
            String sql = "SELECT * FROM JDBC_und_DAO.students WHERE EXTRACT(YEAR FROM gebdate) like ?  ";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentArrayList = new ArrayList<>();
            while (resultSet.next()){
                studentArrayList.add(
                        new Student(
                                resultSet.getLong("id"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getDate("gebdate")
                        )
                );
            }
            return studentArrayList;

        }catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> getStudentsWithFirstName(String firstName) {
        try {
            String sql = "SELECT * FROM JDBC_und_DAO.students WHERE firstname like ?  ";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%"+firstName+"%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentArrayList = new ArrayList<>();
            while (resultSet.next()){
                studentArrayList.add(
                        new Student(
                                resultSet.getLong("id"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getDate("gebdate")
                        )
                );
            }
            return studentArrayList;

        }catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }
    }

    private int countStudentsInDBWithId(Long id){
        try {
            String sql = "SELECT COUNT(*) FROM JDBC_und_DAO.students WHERE id = ?;";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int studentCount = resultSet.getInt(1);
            return studentCount;
        }
        catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }
    }
}
