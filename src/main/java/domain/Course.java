package domain;
import Exceptions.InvalidValidValueException;

import java.lang.annotation.IncompleteAnnotationException;
import java.sql.Date;

public class Course extends BaseEntity {
    private String name;
    private String description;
    private int hours;
    private Date beginDate;
    private Date endDate;
    private CourseType courseType;

    public Course(Long id, String name, String description, int hours, Date beginDate, Date endDate, CourseType courseType) throws InvalidValidValueException {
        super(id);
        this.setName(name);
        this.setDescription(description);
        this.setHours(hours);
        this.setBeginDate(beginDate);
        this.setEndDate(endDate);
        this.setCourseType(courseType);
    }

    public Course(String name, String description, int hours, Date beginDate, Date endDate, CourseType courseType) throws InvalidValidValueException {
        super(null);
        this.setName(name);
        this.setDescription(description);
        this.setHours(hours);
        this.setBeginDate(beginDate);
        this.setEndDate(endDate);
        this.setCourseType(courseType);
    }

    public String getName() {
        return name;
    }

    public void setName(String name)throws InvalidValidValueException {
        if(name != null && name.length() > 1){
            this.name = name;
        }else {
            throw new InvalidValidValueException("Name darf nicht leer sein und laenger als 1 sein");
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) throws InvalidValidValueException{
        if(description != null && description.length() > 1){
            this.description = description;
        }else {
            throw new InvalidValidValueException("Description darf nicht leer sein und laenger als 1 sein");
        }
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) throws InvalidValidValueException{
        if(hours > 0){
            this.hours = hours;
        }else {
            throw new InvalidValidValueException("Stunden duerfen nicht leer sein und muessen groesser als 1 sein");
        }

    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) throws InvalidValidValueException{
        if( beginDate != null){
            if(this.endDate!=null){
                if(beginDate.before(endDate)){
                    this.beginDate = beginDate;
                }
                else{
                    throw new InvalidValidValueException("Kursbeginn muss vor Endbeginn sein");
                }
            }
            else{
                this.beginDate = beginDate;
            }
        }
        else{
            throw new InvalidValidValueException("Das BeginnDate darf nicht null sein");
        }
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) throws InvalidValidValueException {
        if( endDate != null){
            if(this.beginDate!=null){
                if(endDate.after(this.beginDate)){
                    this.endDate = endDate;
                }
                else{
                    throw new InvalidValidValueException("Kursende muss nach beginndatum sein");
                }
            }
            else{
                this.endDate = endDate;
            }
        }
        else{
            throw new InvalidValidValueException("Das EndDate darf nicht null sein");
        }
    }

    public CourseType getCourseType() {
        return courseType;
    }

    public void setCourseType(CourseType courseType) throws InvalidValidValueException {
        if(courseType != null){
            this.courseType = courseType;
        }else{
            throw new InvalidValidValueException("Kurstyp darf nicht Null sein");
        }
    }

    @Override
    public String toString() {
        return "Course{" +
                "id = "+ getId() + '\''+
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", hours=" + hours +
                ", beginDate=" + beginDate +
                ", endDate=" + endDate +
                ", courseType=" + courseType +
                '}';
    }

}
