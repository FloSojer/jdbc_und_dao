import DataAccess.MySQLCourseRepository;
import DataAccess.MySQLDatabaseConnection;
import DataAccess.MySQLStudentRepository;
import DataAccess.StudenRepository;
import ui.Cli;

import java.sql.Connection;
import java.sql.SQLException;

public class main {
    public static void main(String[] args){
        System.out.println("TEstiTest");
        try{
            Cli cli = new Cli(new MySQLCourseRepository(), new MySQLStudentRepository());
            cli.start();
        } catch (SQLException e) {
            System.out.println("Datenbankfehler "+e.getMessage() +"SQL Fehler"+ e.getSQLState());
        } catch (ClassNotFoundException e) {
            System.out.println("Datenbankfehler "+e.getMessage());
        }

    }
}
