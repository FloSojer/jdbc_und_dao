package Exceptions;

public class InvalidValidValueException extends RuntimeException {
    public InvalidValidValueException(String message) {
        super(message);
    }
}
