package domain;

public enum CourseType {
    OE, BF, ZA, SW, RU, FF
}
