package DataAccess;

import domain.Course;
import domain.CourseType;
import util.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySQLCourseRepository implements MyCourseRepository{
    private Connection con;
    private String url = "jdbc:mysql://0.0.0.0:3306/JDBC_und_DAO";
    private String user = "root";
    private String pwd = "DieBohne";

    public MySQLCourseRepository() throws SQLException, ClassNotFoundException {
        this.con = MySQLDatabaseConnection.getConnection(this.url, this.user, this.pwd);
    }
    @Override
    public Optional<Course> insert(Course entity) {
        Assert.notNull(entity);
        // id = null + name + description + hours + begindate + enddate + CourseType
        String sql = "INSERT INTO JDBC_und_DAO.courses VALUES (null, ?, ?, ?, ?, ?, ?);";
        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getDescription());
            preparedStatement.setInt(3, entity.getHours());
            preparedStatement.setDate(4, entity.getBeginDate());
            preparedStatement.setDate(5, entity.getEndDate());
            preparedStatement.setString(6, entity.getCourseType().toString());

            int effectedRows = preparedStatement.executeUpdate();
            if(effectedRows == 0){
                return Optional.empty();
            }else {
                ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                 if(generatedKeys.next()){
                     return this.getByID(generatedKeys.getLong(1));
                 }else {
                     return Optional.empty();
                 }
            }

        }catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }

       // return Optional.empty();
    }

    @Override
    public Optional<Course> getByID(Long id) {
        Assert.notNull(id);
        if(countCorsesInDBWithId(id)==0){
            return Optional.empty();
        }
        else {
            String sql = "SELECT * FROM JDBC_und_DAO.courses WHERE id = ?;";
            try {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                Course course = new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("begindate"),
                        resultSet.getDate("enddate"),
                        CourseType.valueOf(resultSet.getString("coursetype")
                ));
                return Optional.of(course);
            }
            catch (SQLException sqlException){
               throw new DataBaseException(sqlException.getMessage());
            }
        }

    }

    private int countCorsesInDBWithId(Long id){
        try {
            String sql = "SELECT COUNT(*) FROM JDBC_und_DAO.courses WHERE id = ?;";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int courseCount = resultSet.getInt(1);
            return courseCount;
        }
        catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }
    }
    @Override
    public List<Course> getAll() {
        String sql = "SELECT * FROM JDBC_und_DAO.courses; ";
        try{
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Course> courseList = new ArrayList<>();
            while (resultSet.next()){
                courseList.add( new Course(
                                resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getString("description"),
                                resultSet.getInt("hours"),
                                resultSet.getDate("begindate"),
                                resultSet.getDate("enddate"),
                                CourseType.valueOf(resultSet.getString("coursetype"))
                        )
                );

            }
            return courseList;
        }catch (SQLException e) {
            throw new DataBaseException("Databaseerror Occured");
        }
    }

    @Override
    public Optional<Course> update(Course entity) {

        Assert.notNull(entity);
        String sql = "UPDATE JDBC_und_DAO.courses SET name = ?, description =  ?, hours = ?, begindate = ?, enddate = ?, coursetype = ? WHERE id = ?;";
        if(countCorsesInDBWithId(entity.getId()) == 0){
            return Optional.empty();
        }else{
        try{
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getDescription());
            preparedStatement.setInt(3, entity.getHours());
            preparedStatement.setDate(4, entity.getBeginDate());
            preparedStatement.setDate(5, entity.getEndDate());
            preparedStatement.setString(6, entity.getCourseType().toString());
            preparedStatement.setLong(7, entity.getId());
            int affectedRows = preparedStatement.executeUpdate();

            if(affectedRows == 0){
                return Optional.empty();
            }else {
                return this.getByID(entity.getId());
            }

        }catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }
        }
    }

    @Override
    public void deleteById(Long id) {
        Assert.notNull(id);
        try {
            String sql = "DELETE FROM JDBC_und_DAO.courses WHERE id = ?";
            if (countCorsesInDBWithId(id) == 1) {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong( 1, id);
                preparedStatement.executeUpdate();

            }
        } catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Course> findCourseByName(String name) {
        return null;
    }

    @Override
    public List<Course> findAllCourseByDescription(String description) {
        return null;
    }

    @Override
    public List<Course> findAllCourseByNameOrDescription(String searchtext) {

        try {
            String sql = "SELECT * FROM JDBC_und_DAO.courses WHERE LOWER(`description`) LIKE LOWER(?) OR LOWER(`name`) LIKE LOWER(?)  ";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%"+searchtext+ "%");
            preparedStatement.setString(2, "%"+searchtext + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Course> courseArrayList = new ArrayList<>();
            while (resultSet.next()){
                    courseArrayList.add(new Course(
                            resultSet.getLong("id"),
                            resultSet.getString("name"),
                            resultSet.getString("description"),
                            resultSet.getInt("hours"),
                            resultSet.getDate("begindate"),
                            resultSet.getDate("enddate"),
                            CourseType.valueOf(resultSet.getString("coursetype"))
                    ));
            }
            return courseArrayList;

        }catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Course> findAllCourseByCourseType(CourseType courseType) {
        return null;
    }

    @Override
    public List<Course> findAllCourseByStartDate(Date startDate) {
        return null;
    }

    @Override
    public List<Course> findAllRunningCourses() {
        try {
            String sql = "SELECT * FROM JDBC_und_DAO.courses WHERE NOW()<`enddate`";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Course> courseArrayList = new ArrayList<>();
            while (resultSet.next()){
                courseArrayList.add(new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("begindate"),
                        resultSet.getDate("enddate"),
                        CourseType.valueOf(resultSet.getString("coursetype"))
                ));
            }
            return courseArrayList;

        }catch (SQLException sqlException){
            throw new DataBaseException(sqlException.getMessage());
        }
    }
}
